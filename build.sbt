lazy val commonSettings = Seq(
  scalaVersion := "2.12.8",
  licenses := Seq("MIT" -> url("http://opensource.org/licenses/MIT")),
  organization := "com.bitbucket.gafiatulin",
  developers += Developer("", "Victor Gafiatulin", "gafiatulin@gmail.com", new URL("https://bitbucket.com/gafiatulin")),
  scmInfo := Option(ScmInfo(
    url("https://bitbucket.com/gafiatulin/akka-streams-ext/"),
    "scm:git:bitbucket.com:gafiatulin/akka-streams-ext.git"
  )),
  scalacOptions ++= Seq(
    "-deprecation",
    "-encoding", "UTF-8",
    "-feature",
    "-language:existentials",
    "-language:higherKinds",
    "-language:implicitConversions",
    "-language:postfixOps",
    "-language:reflectiveCalls",
    "-unchecked",
    "-Xfatal-warnings",
    "-Xlint",
    "-Yno-adapted-args",
    "-Ywarn-dead-code",
    "-Ywarn-numeric-widen",
    "-Ywarn-value-discard",
    "-Xfuture",
    "-Ywarn-unused-import"
  ),
  libraryDependencies ++= Seq("com.typesafe.akka" %% "akka-stream" % "2.5.22" % "provided", "org.agrona" % "agrona" % "0.9.31"),
  autoScalaLibrary := false,
  publishMavenStyle := true,
  publishTo in ThisBuild := Some(Resolver.file("file",  new File(Path.userHome.absolutePath+"/Workspace/mvn-repo")))
)

lazy val core = (project in file("core")).settings(commonSettings, Seq(name := "akka-streams-ext", version := "0.5.11"))

lazy val nats = (project in file("nats")).settings(commonSettings, Seq(name := "akka-streams-ext-nats", version := "0.3.14"))

publishArtifact := false