package com.bitbucket.gafiatulin.stream.nats

import com.typesafe.config.Config
import io.nats.client.{ConnectionListener, ErrorListener}
import io.nats.streaming.{NatsStreaming, Options, StreamingConnection}

trait StreamingConnectionProvider{
  def connection(errorListener: Option[ErrorListener], connectionListner: Option[ConnectionListener]): StreamingConnection
}

final case class NatsStreamingConnectionBuilder(clusterId: String, clientId: String, optionsBuilder: Options.Builder) extends StreamingConnectionProvider{
  def connection(errorListener: Option[ErrorListener], connectionListner: Option[ConnectionListener]): StreamingConnection = {
    val withEL = errorListener.fold(optionsBuilder)(optionsBuilder.errorListener)
    NatsStreaming.connect(clusterId, clientId, connectionListner.fold(withEL)(withEL.connectionListener).build())
  }
}

object NatsStreamingConnectionBuilder{
  def fromConfig(config: Config): NatsStreamingConnectionBuilder = fromSettings(NatsStreamingConnectionSettings.fromConfig(config))
  def fromSettings(settings: NatsStreamingConnectionSettings): NatsStreamingConnectionBuilder = {
    val b = new Options.Builder().natsUrl(settings.url)
    val bConT = settings.connectionTimeout.map(b.connectWait).getOrElse(b)
    val bPubAckT = settings.publishAckTimeout.map(bConT.pubAckWait).getOrElse(bConT)
    val bMaxInF = settings.publishMaxInFlight.map(bPubAckT.maxPubAcksInFlight).getOrElse(bPubAckT)
    val options = settings.discoverPrefix.map(bMaxInF.discoverPrefix).getOrElse(bMaxInF)
    NatsStreamingConnectionBuilder(settings.clusterId, settings.clientId, options)
  }
}
