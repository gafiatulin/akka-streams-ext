package com.bitbucket.gafiatulin.stream.nats

import java.time.{Duration, Instant}

import com.typesafe.config.Config
import io.nats.streaming.SubscriptionOptions

import scala.collection.JavaConverters._
import scala.util.{Failure, Success, Try}


sealed trait DeliveryStartPosition

object DeliveryStartPosition{
  case object OnlyNew extends DeliveryStartPosition
  case object AllAvailable extends DeliveryStartPosition
  case object LastReceived extends DeliveryStartPosition
  case class AfterSequenceNumber(seq: Long) extends DeliveryStartPosition
  case class AfterTime(time: Instant) extends DeliveryStartPosition
  def fromConfig(config: Config): DeliveryStartPosition = Try(config.getLong("deliverAfterSequenceNumber")) match {
    case Success(seq) => AfterSequenceNumber(seq)
    case Failure(_) =>
      Try(Instant.ofEpochMilli(config.getLong("deliverAfterEpochMillis")))
        .orElse(Try(Instant.parse(config.getString("deliverAfterInstance")))) match {
          case Success(time) => AfterTime(time)
          case Failure(_) => config.getString("deliver").toLowerCase match {
            case "onlynew" => OnlyNew
            case "allavailable" => AllAvailable
            case "lastreceived" => LastReceived
          }
      }
  }
}

final case class NatsStreamingConnectionSettings(
  clusterId: String,
  clientId: String,
  url: String,
  connectionTimeout: Option[Duration],
  publishAckTimeout: Option[Duration],
  publishMaxInFlight: Option[Int],
  discoverPrefix: Option[String]
)

case object NatsStreamingConnectionSettings{
  final def url(host: String, port: Int): String = "nats://" + host + ":" + port.toString
  def fromConfig(config: Config): NatsStreamingConnectionSettings =
    NatsStreamingConnectionSettings(
      config.getString("clusterId"),
      config.getString("clientId"),
      Try(config.getString("url")).getOrElse(url(config.getString("host"), config.getInt("port"))),
      Try(config.getDuration("connectionTimeout")).toOption,
      Try(config.getDuration("pubAckTimeout")).toOption,
      Try(config.getInt("pubMaxInFlight")).toOption,
      Try(config.getString("discoverPrefix")).toOption
    )
}

sealed trait NatsStreamingSubscriptionSettings{
  def cp: StreamingConnectionProvider
  def subjects: Seq[String]
  def subscriptionQueue: String
  def durableSubscriptionName: Option[String]
  def startPosition: DeliveryStartPosition
  def subMaxInFlight: Option[Int]
  def bufferSize: Int
  def autoRequeueTimeout: Option[Duration]
  def manualAcks: Boolean
  def closeConnectionAfterStop: Boolean
  def subscriptionOptions: SubscriptionOptions = {
    val b = new SubscriptionOptions.Builder()
    val bMaxInFlight = subMaxInFlight.map(b.maxInFlight).getOrElse(b)
    val bAckWait = autoRequeueTimeout.map(bMaxInFlight.ackWait).getOrElse(bMaxInFlight)
    val bDurable = durableSubscriptionName.map(bAckWait.durableName).getOrElse(bAckWait)
    val builder = if(manualAcks) bDurable.manualAcks() else bDurable
    startPosition match {
      case DeliveryStartPosition.OnlyNew => builder
      case DeliveryStartPosition.AllAvailable => builder.deliverAllAvailable
      case DeliveryStartPosition.LastReceived => builder.startWithLastReceived
      case DeliveryStartPosition.AfterSequenceNumber(seq) => builder.startAtSequence(seq)
      case DeliveryStartPosition.AfterTime(time) => builder.startAtTime(time)
    }
  }.build()
}

object NatsStreamingSubscriptionSettings{
  val defaultBufferSize = 16384
}

final case class SimpleSubscriptionSettings(
  cp: StreamingConnectionProvider,
  subjects: Seq[String],
  subscriptionQueue: String,
  durableSubscriptionName: Option[String],
  startPosition: DeliveryStartPosition,
  subMaxInFlight: Option[Int],
  bufferSize: Int,
  autoRequeueTimeout: Option[Duration],
  manualAcks: Boolean,
  closeConnectionAfterStop: Boolean
) extends NatsStreamingSubscriptionSettings

case object SimpleSubscriptionSettings{
  def fromConfig(config: Config, connectionProvider: StreamingConnectionProvider): SimpleSubscriptionSettings =
    SimpleSubscriptionSettings(
      connectionProvider,
      Try(config.getStringList("subjects").asScala)
        .orElse(Try(config.getString("subjects").split(',').map(_.trim).toSeq))
        .orElse(Try(config.getStringList("subject").asScala))
        .orElse(Try(config.getString("subject").split(',').map(_.trim).toSeq))
        .getOrElse(Seq(config.getString("subject"))),
      config.getString("subscriptionQueue"),
      Try(config.getString("durableSubscriptionName")).toOption,
      Try(DeliveryStartPosition.fromConfig(config)).getOrElse(DeliveryStartPosition.OnlyNew),
      Try(config.getInt("subscriptionMaxInFlight")).toOption,
      Try(config.getInt("bufferSize")).getOrElse(NatsStreamingSubscriptionSettings.defaultBufferSize),
      Try(config.getDuration("autoRequeueTimeout")).toOption,
      Try(config.getBoolean("manualAcks")).getOrElse(true),
      Try(config.getBoolean("closeConnectionAfterStop")).getOrElse(true)
    )
  def fromConfig(config: Config): SimpleSubscriptionSettings = fromConfig(config, NatsStreamingConnectionBuilder.fromConfig(config))
}

final case class SubscriptionWithAckSettings(
  cp: StreamingConnectionProvider,
  subjects: Seq[String],
  subscriptionQueue: String,
  durableSubscriptionName: Option[String],
  startPosition: DeliveryStartPosition,
  subMaxInFlight: Option[Int],
  manualAckTimeout: Duration,
  autoRequeueTimeout: Option[Duration],
  bufferSize: Int,
  manualAcks: Boolean,
  closeConnectionAfterStop: Boolean
) extends NatsStreamingSubscriptionSettings

case object SubscriptionWithAckSettings{
  def fromConfig(config: Config, connectionProvider: StreamingConnectionProvider): SubscriptionWithAckSettings = {
    val simple = SimpleSubscriptionSettings.fromConfig(config, connectionProvider)
    SubscriptionWithAckSettings(
      simple.cp,
      simple.subjects,
      simple.subscriptionQueue,
      simple.durableSubscriptionName,
      simple.startPosition,
      simple.subMaxInFlight,
      config.getDuration("manualAckTimeout"),
      Some(config.getDuration("autoRequeueTimeout")),
      simple.bufferSize,
      manualAcks = true,
      simple.closeConnectionAfterStop
    )
  }
  def fromConfig(config: Config): SubscriptionWithAckSettings =
    fromConfig(config, NatsStreamingConnectionBuilder.fromConfig(config))
}

final case class PublishingSettings(
  cp: StreamingConnectionProvider,
  defaultSubject: String,
  parallel: Boolean,
  closeConnectionAfterStop: Boolean
)

case object PublishingSettings{
  def fromConfig(config: Config): PublishingSettings =
  PublishingSettings(
    NatsStreamingConnectionBuilder.fromConfig(config),
    config.getString("defaultSubject"),
    config.getBoolean("parallel"),
    Try(config.getBoolean("closeConnectionAfterStop")).getOrElse(true)
  )
}
