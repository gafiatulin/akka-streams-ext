package com.bitbucket.gafiatulin.stream

import akka.Done

import scala.concurrent.Future

trait CompletableQueue[T]{
  def offer(v: T): Boolean
  def complete(): Boolean
  def fail(ex: Throwable): Boolean
  def completion: Future[Done]
  def currentSize: Int
}

