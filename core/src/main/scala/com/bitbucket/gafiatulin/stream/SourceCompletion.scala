package com.bitbucket.gafiatulin.stream

private [stream] object SourceCompletion{
  sealed trait Input[+T]
  case object Completion extends Input[Nothing]
  final case class Failure(ex: Throwable) extends Input[Nothing]
}