package com.bitbucket.gafiatulin.stream

import akka.Done
import akka.stream.scaladsl.Source
import akka.stream.stage.{AsyncCallback, GraphStageLogic, GraphStageWithMaterializedValue, OutHandler}
import akka.stream.{Attributes, Outlet, SourceShape}
import com.bitbucket.gafiatulin.stream.SourceCompletion.{Completion, Failure, Input}
import org.agrona.concurrent.{ManyToOneConcurrentArrayQueue, ManyToOneConcurrentLinkedQueue}

import scala.concurrent.{Future, Promise}

private abstract class ConcurrentQueueSourceStageLogic[T](
  promise: Promise[Done],
  shape: SourceShape[T],
  out: Outlet[T]
) extends GraphStageLogic(shape) with OutHandler with CompletableQueue[T]{
  private final var downstreamWaiting = false
  private final var initialized = false
  private final var completionLogic: AsyncCallback[Input[T]] = _
  private final var processingLogic: AsyncCallback[Unit] = _
  private final val completing = new java.util.concurrent.atomic.AtomicBoolean(false)
  private final val scheduled = new java.util.concurrent.atomic.AtomicBoolean(false)
  protected val buffer: java.util.Queue[T]

  private final def completeWith(c: Input[T]): Boolean = if(completing.compareAndSet(false, true) && initialized){
    completionLogic.invoke(c)
    true
  } else if(initialized) true else false

  private final def handleCompletion(c: Input[T]): Unit = c match {
    case Completion =>
      while(!buffer.isEmpty){
        val e = buffer.poll()
        if(null != e) emit(out, e)
      }
      promise.trySuccess(Done)
      completeStage()
    case Failure(ex) =>
      promise.tryFailure(ex)
      failStage(ex)
  }

  private final def process(u: Unit): Unit = {
    if (!scheduled.compareAndSet(true, false)) throw new IllegalStateException("Code should never reach here")
    if (downstreamWaiting && (!buffer.isEmpty)) {
      val e = buffer.poll()
      if(null != e){
        downstreamWaiting = false
        push(out, e)
      }
    }
  }

  final def offer(v: T): Boolean = if(completing.get){
    false
  } else {
    val b = buffer.offer(v)
    if(initialized && scheduled.compareAndSet(false, true)){
      processingLogic.invoke(())
    }
    b
  }

  final def currentSize: Int = buffer.size()

  final def complete(): Boolean = completeWith(SourceCompletion.Completion)

  final def fail(ex: Throwable): Boolean = completeWith(SourceCompletion.Failure(ex))

  final def completion: Future[Done] = promise.future

  override def preStart(): Unit = {
    completionLogic = getAsyncCallback(handleCompletion)
    processingLogic = getAsyncCallback(process)
    initialized = true
    if (scheduled.compareAndSet(false, true)) processingLogic.invoke(())
    super.preStart()
  }

  override def postStop(): Unit = {
    completing.set(true)
    while(!buffer.isEmpty){
      buffer.poll()
    }
    promise.tryFailure(new Exception())
    super.postStop()
  }

  override def onDownstreamFinish(): Unit = {
    promise.trySuccess(Done)
    super.onDownstreamFinish()
  }

  override def onPull(): Unit = if (buffer.isEmpty) {
    downstreamWaiting = true
  } else {
    val e = buffer.poll()
    if(null == e) {
      downstreamWaiting = true
    } else {
      push(out, e)
    }
  }

  setHandler(out, this)
}

private final class ConcurrentBoundedQueueSourceStageLogic[T](
  size: Int,
  promise: Promise[Done],
  shape: SourceShape[T],
  out: Outlet[T]
) extends ConcurrentQueueSourceStageLogic(promise, shape, out){
  override protected val buffer = new ManyToOneConcurrentArrayQueue[T](size)
}

private final class ConcurrentUnboundedQueueSourceStageLogic[T](
  promise: Promise[Done],
  shape: SourceShape[T],
  out: Outlet[T]
) extends ConcurrentQueueSourceStageLogic(promise, shape, out){
  override protected val buffer = new ManyToOneConcurrentLinkedQueue[T]
}

private [stream] final class ConcurrentBoundedQueueSourceStage[T](size: Int) extends GraphStageWithMaterializedValue[SourceShape[T], CompletableQueue[T]]{
  val out: Outlet[T] = Outlet("ConcurrentBoundedQueueSource.out")
  val shape: SourceShape[T] = SourceShape(out)
  def createLogicAndMaterializedValue(inheritedAttributes: Attributes): (GraphStageLogic, CompletableQueue[T]) = {
    val promise = Promise[Done]()
    val logic = new ConcurrentBoundedQueueSourceStageLogic[T](size, promise, shape, out)
    (logic, logic: CompletableQueue[T])
  }
}

private [stream] final class ConcurrentUnboundedQueueSourceStage[T] extends GraphStageWithMaterializedValue[SourceShape[T], CompletableQueue[T]]{
  val out: Outlet[T] = Outlet("ConcurrentBoundedQueueSource.out")
  val shape: SourceShape[T] = SourceShape(out)
  def createLogicAndMaterializedValue(inheritedAttributes: Attributes): (GraphStageLogic, CompletableQueue[T]) = {
    val promise = Promise[Done]()
    val logic = new ConcurrentUnboundedQueueSourceStageLogic[T](promise, shape, out)
    (logic, logic: CompletableQueue[T])
  }
}

case object ConcurrentQueueSource{
  def apply[T]: Source[T, CompletableQueue[T]] = Source.fromGraph(new ConcurrentUnboundedQueueSourceStage[T])
  def apply[T](bufferSize: Int) : Source[T, CompletableQueue[T]] = Source.fromGraph(new ConcurrentBoundedQueueSourceStage[T](bufferSize))
}