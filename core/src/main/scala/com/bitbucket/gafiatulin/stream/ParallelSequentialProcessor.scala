package com.bitbucket.gafiatulin.stream

import akka.{Done, NotUsed}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{BroadcastHub, Flow, Keep, Sink, Source}

import scala.concurrent.Future

trait PartitionBy[-A, +B] { def apply(obj: A): B }

class ParallelSequentialProcessor[T1, T2, T3] private (source: Source[T1, T3], process: PartialFunction[T1, Future[Done]], bhBufferSize: Int)
                                                      (implicit f: PartitionBy[T1, T2], materializer: ActorMaterializer)
{
  private final val set: java.util.HashSet[T2] = new java.util.HashSet[T2]()
  private def processingFlow(p: T2) = Flow[T1].filter(f(_) == p).mapAsync(1)(process).to(Sink.ignore)
  private def broadcastHubSource: () => Source[T1, NotUsed] = () => broadcastHub
  private val (m1, broadcastHub) = source.viaMat(Flow[T1].map{e =>
    val p = f(e)
    if(!set.contains(p)) {
      broadcastHubSource().runWith(processingFlow(p))
      set.add(p)
    }
    e
  })(Keep.left).toMat(BroadcastHub.sink(bhBufferSize))(Keep.both).run()
  final def materializedSource: () => T3 = () => m1
}

case object ParallelSequentialProcessor{
  def run[T1, T2, T3](source: Source[T1, T3], process: PartialFunction[T1, Future[Done]], bhBufferSize: Int = 256)
                     (implicit f: PartitionBy[T1, T2], materializer: ActorMaterializer) =
    new ParallelSequentialProcessor[T1, T2, T3](source, process, bhBufferSize)
}